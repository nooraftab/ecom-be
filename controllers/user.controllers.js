const express = require('express');
const bcrypt = require('bcryptjs');
const config = require('config');
const jwt = require('jsonwebtoken');

const User = require('../models/User')

class Controllers{

    constructor(){
        
    }

    async registerUser(req, res){
        const {
            name,
            username,
            password
        } = req.body;

        let errors = []

        if(!name){
            errors.push('Name');
        }
        if(!username){
            errors.push('Username');
        }
        if(!password){
            errors.push('Password');
        }

        if(errors.length > 0){
            errors = errors.join(',');
            return res.json({
                message: `These are required fields: ${errors}.`,
                status: false
            })
        }
        
        try {
            let user = await User.findOne({username})
            
            if (user) {
                return res.status(400).json({'msg': 'User already exists'})
            } else {
                user = new User({
                    name,
                    username,
                    password
                })
    
                let salt = await bcrypt.genSalt(10);
                user.password = await bcrypt.hash(password, salt);
    
                await user.save();
    
                const payload = {
                    user:{
                        id: user.id,
                        userRole : user.userRole
                    }
                }
    
                jwt.sign(payload, config.get('jwtSecret'), {
                    expiresIn: 360000
                }, (err, token) => {
                    if (err) throw err;
                    res.status(200).send({
                        token: token,
                        message: 'Successfully Added',
                        status: 'success',
                        data: user
                    })
                });
            }
        } catch (err) {
            console.error(err.message);
            res.status(500).send('Server Error')
        }
    }

    async login(req, res){
        const {username, password} = req.body;

        let errors = []

        if(!username){
            errors.push('Username');
        }   
        if(!password){
            errors.push('Password');
        }
        if(errors.length > 0){
            errors = errors.join(',');
            return res.json({
                message: `These are required fields: ${errors}.`,
                status: false
            })
        }
        try {
            let user = await User.findOne({username})
    
            if(!user){
               return res.status(400).json({msg: 'Invalid Credentials'})
            }
    
            const isMatch = await bcrypt.compare(password, user.password)
    
            if(!isMatch){
                return res.status(400).json({msg: 'Invalid Credentials'})
            }
    
            const payload = {
                user:{
                    id: user.id,
                    userRole: user.userRole
                }
            }
    
            jwt.sign(payload, config.get('jwtSecret'), {
                expiresIn: 360000
            }, (err, token) => {
                if (err) throw err;
                res.json({token, userRole:user.userRole})
            });
    
        } catch (err) {
            console.error(err.message);
            res.status(500).send( 'Server Error')
        }
    }

}

module.exports = new Controllers();