const express = require('express');
var cors = require('cors')
var bodyParser = require('body-parser');
const connectDB = require('./config/database');


const app = express();
app.use(cors())

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
app.use(express.json({extended: false}))

connectDB();
require('./routes')(app);


const PORT = process.env.PORT || 3000;
app.listen(PORT, () => console.log(`Server Started on port ${PORT}`));