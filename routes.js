const user = require('./controllers/user.controllers')


module.exports = function(app){

    app.post('/login', user.login)
    app.post('/register/user', user.registerUser)
}